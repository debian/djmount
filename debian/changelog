djmount (0.71-8) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:48:56 +0200

djmount (0.71-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use HTTPS for the Vcs-* fields.
  * Bump debhelper compat level to 10, so that dh_update_autotools_config is run
    instead of the external addon dh_autotools-dev (but disable autoreconf).
  * Use dh_auto_configure instead of manually calling ./configure.
  * Call dh_auto_install instead of manually calling `make install`.
  * Stop using hardening-wrapper, make use of dpkg-buildflags with
        DEB_BUILD_MAINT_OPTIONS=hardening=+all
    instead.  Closes: #836753
  * Fix copyright-format 1.0 syntax.
    + dep5-copyright-license-name-not-unique by moving GPL-2+ license text to a
      standalone license paragraph.
    + invalid-short-name-in-dep5-copyright by renaming the name of the BSD
      license from 'BSD' to 'BSD-3-Clauses'.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 27 Sep 2016 21:14:45 +0000

djmount (0.71-7) unstable; urgency=medium

  * debian/patches:
    + Added: 004-avoid-crash-by-using-size_t.patch:
      - Fixes segfault on 64-bit architectures when reading files
        from a mounted DLNA share (Closes: #674753, #701680)
        Thanks to Bernhard Übelacker <bernhardu@vr-web.de> and
        John Paul Adrian Glaubitz <glaubitz@physik.fu-berlin.de>

 -- Dario Minnucci <midget@debian.org>  Sun, 21 Dec 2014 19:14:14 +0100

djmount (0.71-6) unstable; urgency=low

  * debian/control:
    + Bump Standards-Version to 3.9.4
    + Change dependency from 'fuse-utils' to 'fuse'. fuse-utils was
      dropped from sid turning djmount uninstallable. (Closes: #698121)
  * debian/rules:
    + Added build hardening flags

 -- Dario Minnucci <midget@debian.org>  Mon, 21 Jan 2013 20:55:07 +0100

djmount (0.71-5) unstable; urgency=low

  * debian/patches:
    - Added: 003-support-fstab-mounting.diff:
      - Adds support for mounting via /etc/fstab.
        Thanks to Kevin Vargo. (Closes: #660098)

 -- Dario Minnucci <midget@debian.org>  Sat, 21 Apr 2012 16:56:57 +0200

djmount (0.71-4) unstable; urgency=low

  * Switch to dpkg-source 3.0 (quilt) format
  * debian/control:
    - Repository converted from svn to git.
      Updated Vcs-Browser and Vcs-Git fields.
    - Removed Build-Depends on quilt
    - Updated debhelper compatibility to >= 9.
    - Bump Standards-Version to 3.9.3
    - Use libupnp-dev instead of libupnp3-dev
  * debian/rules:
    - Removed dependencies on quilt
  * debian/copyright:
    - Updated format specification to rev=135
    - Pointer to BSD licensed replaced with the license text itself
      to suppress lintian warning.
      W: copyright-refers-to-deprecated-bsd-license-file
  * debian/rules: Rewritten to use new format
  * debian/patches:
    - Added 002-libupnp-1.6.13.diff for transition from libupnp3 to
      libupnp6. Thanks to Nick Leverton. (Closes: #656832)

 -- Dario Minnucci <midget@debian.org>  Tue, 03 Apr 2012 01:00:25 +0200

djmount (0.71-3) unstable; urgency=high

  * Applied patch 001-libupnp-1.6.6.diff to handle renaming of function
    SetLogFileNames to UpnpSetLogFileNames.
    Thanks to Nick Leverton for the patch. (Closes: #581879, #582989)
  * debian/control: Bump Standards-Version to 3.8.4
  * debian/rules: Changes to get rid of lintian
    W: patch-system-but-direct-changes-in-diff
  * Added debian/source/format file.

 -- Dario Minnucci <midget@debian.org>  Tue, 25 May 2010 14:26:24 +0200

djmount (0.71-2) unstable; urgency=low

  * Patch system migrated from dpatch to quilt
  * debian/control:
    - Added quilt support
    - Maintainer's address updated.
    - According to Policy 'Section 2.5: Priorities', priority was updated to
      extra because depends on 'libupnp3'
  * debian/copyright: gl/* copyright fixed. Thanks to Chris Lamb.
  * debian/rules: config.{sub,guess} updated. (Closes: #546298)

 -- Dario Minnucci <midget@debian.org>  Sun, 24 Jan 2010 03:14:54 +0100

djmount (0.71-1) unstable; urgency=low

  * Initial release (Closes: #517762)
  * debian/rules: Compiled against Debian libupnp-dev and libtalloc-dev
  * debian/README.source: Added
  * Improvements on package descrptions and manpage:
    Thanks to MJ Ray and Justin B Rye.
    (http://lists.debian.org/debian-l10n-english/2009/08/msg00074.html)
  * debian/patches/01_manpages.dpatch: Adds a simple manpage not provided by
    upstream sources. (djmount.1 was sent to upstream to be included).
  * debian/control: Added 'Vcs-Browser' and 'Vcs-Svn' fields
  * debian/patches/01_manpages.dpatch: Adapted to DEP3.
    (see: http://dep.debian.net/deps/dep3/ for details)
  * Get rid of lintian W: binary-or-shlib-defines-rpath:
     debian/control: Added chrpath to Build-Depends
     debian/rules: Use chrpath after installing to delete current rpath setting.
  * debian/copyright: Added a few improvements.

 -- Dario Minnucci (midget) <debian@midworld.net>  Fri, 11 Sep 2009 01:58:45 +0200
